" **************************************************************************** "
"                                                                              "
"        _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___         "
"        _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|        "
"       |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_         "
"       |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|        "
"        _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___         "
"       | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|        "
"       |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_         "
"        _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|        "
"                                                                              "
"   example.vim                                                                "
"                                                                              "
"   By: 4nzar <sadik.essaidi@gmail.com>                                        "
"                                                                              "
"   Created: 09/05/2023 12:23:28 by 4nzar                                      "
"   Updated: 09/05/2023 12:31:24 by 4nzar                                      "
"                                                                              "
" **************************************************************************** "

let g:header_field_ascii = [
	\" _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___ ",
	\" _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|",
	\"|  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_ ",
	\"|_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|",
	\" _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___ ",
	\"| |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|",
	\"|_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_ ",
	\" _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|",
]

let g:header_auto_insert = 1
let g:header_auto_update = 1

let g:header_field_author_name = "jdoe"
let g:header_field_author_email = "john.doe@email.com"

" this will generate a header like this for a vimscript file
" **************************************************************************** "
"                                                                              "
"        _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___         "
"        _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|        "
"       |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_         "
"       |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|        "
"        _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___         "
"       | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|        "
"       |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_         "
"        _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|        "
"                                                                              "
"   example.vim                                                                "
"                                                                              "
"   By: jdoe <john.doe@email.com>                                              "
"                                                                              "
"   Created: 09/05/2023 12:23:28 by jdoe                                       "
"   Updated: 09/05/2023 12:27:56 by jdoe                                       "
"                                                                              "
" **************************************************************************** "


