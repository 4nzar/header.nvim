" **************************************************************************** "
"                                                                              "
"        _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___         "
"        _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|        "
"       |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_         "
"       |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|        "
"        _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___         "
"       | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|        "
"       |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_         "
"        _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|        "
"                                                                              "
"   header.vim                                                                 "
"                                                                              "
"   By: 4nzar <sadik.essaidi@gmail.com>                                        "
"                                                                              "
"   Created: 08/05/2023 19:07:53 by 4nzar                                      "
"   Updated: 08/05/2023 19:10:28 by 4nzar                                      "
"                                                                              "
" **************************************************************************** "

" START
" Prevents the plugin from being loaded multiple times. If the loaded
" variable exists, do nothing more. Otherwise, assign the loaded
" variable and continue running this instance of the plugin.
if exists('g:loaded_header')
    finish
endif

let s:save_cpo = &cpo " save user coptions
set cpo&vim " reset them to defaults

" Set default global values
"
if !exists("g:header_field_author_name")
    let g:header_field_author_name = ""
endif

if !exists("g:header_field_author_mail")
    let g:header_field_author_mail = ""
endif

if !exists("g:header_field_time_format_creation")
    let g:header_field_time_format_creation = "%d/%m/%Y %X"
endif

if !exists("g:header_field_time_format_modification")
    let g:header_field_time_format_modification = "%d/%m/%Y %X"
endif

if !exists("g:header_field_ascii")
    let g:header_field_ascii = []
endif

if !exists("g:header_auto_insert")
    let g:header_auto_insert = 0
endif

if !exists("g:header_auto_update")
    let g:header_auto_update = 0
endif

if !exists("g:header_frame_width")
    let g:header_frame_width = 80
endif

if !exists("g:header_license_type")
    let g:header_license_type = ""
endif

if !exists("g:header_debug")
    let g:header_debug = 0
endif

command! -nargs=0 StdHeader lua require("header").set()

" Set default global values
if exists('g:header_auto_insert') && g:header_auto_insert == 1
	autocmd BufNewFile * :StdHeader
endif
if exists('g:header_auto_update') && g:header_auto_update == 1
    autocmd BufWritePost * :StdHeader
endif

let &cpo = s:save_cpo " and restore after
unlet s:save_cpo

let g:loaded_header = 1
" END
