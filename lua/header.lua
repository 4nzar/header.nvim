-- -------------------------------------------------------------------------- --
--                                                                            --
--       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        --
--       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       --
--      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        --
--      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       --
--       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        --
--      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       --
--      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        --
--       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       --
--                                                                            --
--   header.lua                                                               --
--                                                                            --
--   By: 4nzar <sadik.essaidi@gmail.com>                                      --
--                                                                            --
--   Created: 09/05/2023 13:47:11 by 4nzar                                    --
--   Updated: 09/05/2023 13:56:29 by 4nzar                                    --
--                                                                            --
-- -------------------------------------------------------------------------- --

local log = require("utils.log")

local header_config = {}
local M = {}

local bo = vim.bo
local api = vim.api

local function init()

end

M.set = function()
	-- check filetype
	local filetype = string.lower(bo.filetype)
	log.info("filetype = [%s]", filetype)
	if next(header_config) == nil then
		-- get list of configurations
		header_config = require("header.config")
	end
	-- check if a configuration exist for the filetype
	--[[
	header_config.current = header_config.lang[filetype]
	if header_config.current == nil then
		log.error("config not found.")
		return 
	end
	]]--
	-- check that the configuration is valid
	--[[
	for _, field in ipairs({ "head", "tail", "fill" }) do
		if header_config.current[field] == nil then 
			log.error(string.format("field %s missing", field))
			return
		end
	end
	]]--
	-- check if need to update the header else create it
	--[[
	if not header.update(header_config) then
		header.create(header_config)
	end
	]]--
end

return M


