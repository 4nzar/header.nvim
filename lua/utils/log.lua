-- -------------------------------------------------------------------------- --
--                                                                            --
--       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        --
--       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       --
--      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        --
--      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       --
--       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        --
--      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       --
--      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        --
--       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       --
--                                                                            --
--   log.lua                                                                  --
--                                                                            --
--   By: 4nzar <sadik.essaidi@gmail.com>                                      --
--                                                                            --
--   Created: 08/05/2023 19:38:58 by 4nzar                                    --
--   Updated: 08/05/2023 19:38:58 by 4nzar                                    --
--                                                                            --
-- -------------------------------------------------------------------------- --

-- START
local M = {}

--- Log message
local function log(msg, hl)
	if vim.g.header_debug ~= nil and vim.g.header_debug == 1 then
		hl = hl or "String"
		vim.api.nvim_echo({ { "[header.nvim] ", hl }, { msg } }, true, {})
	end
end

--- Warning log message
M.warn = function(msg, args)
	log(string.format(msg, args), "WARNING")
end

--- Error log message
M.error = function(msg, args)
	log(string.format(msg, args), "ERROR")
end

--- Info log message
M.info = function(msg, args)
	log(string.format(msg, args), "INFO")
end

--
return M
-- END
