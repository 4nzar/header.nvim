-- -------------------------------------------------------------------------- --
--                                                                            --
--       _   ___   ___   ___   ___   ___   ___   ___   ___   ___   ___        --
--       _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _| |_  |_|  _|       --
--      |  _  |  _  |_   _|  _  |  _  |  _  |_   _|  _  |  _  |  _  |_        --
--      |_| |_| | |___| |___| | |_| |_| | |___| |___| | |_| |_| | |___|       --
--       _   _  |  ___   ___  |  _   _  |_   _____   _|  _   _  |  ___        --
--      | |_| | |_|  _| |_  |_| | |_| |  _| |_   _| |_  | |_| | |_|  _|       --
--      |_   _|  _  |_   _|  _  |_   _| |  _  | |  _  | |_   _|  _  |_        --
--       _| |___| |___| |___| |___| |_  |_| |_| |_| |_|  _| |___| |___|       --
--                                                                            --
--   configuration.lua                                                        --
--                                                                            --
--   By: 4nzar <sadik.essaidi@gmail.com>                                      --
--                                                                            --
--   Created: 09/05/2023 13:55:15 by 4nzar                                    --
--   Updated: 09/05/2023 14:12:00 by 4nzar                                    --
--                                                                            --
-- -------------------------------------------------------------------------- --

local g = vim.g

local config = {

	field_author_name = "",
	field_author_email = "",

	field_created_format = "%d/%m/%Y %X",

	field_modified_format = "%d/%m/%Y %X",
	field_modified_authored = false,

	field_ascii = {},

	field_license_type = "",

	auto_insert = false,
	auto_update = false,

	frame_width = 80,

	current = {},

	lang = {
		c			= { head = "/*", tail = "*/", fill = "*" },
		cpp			= { head = "/*", tail = "*/", fill = "*" },
		matlab		= { head = "/*", tail = "*/", fill = "*" }, -- m
		nroff		= { head = "/*", tail = "*/", fill = "*" }, -- mm
		swift		= { head = "/*", tail = "*/", fill = "*" },
		javascript	= { head = "/*", tail = "*/", fill = "*" },
		typescript	= { head = "/*", tail = "*/", fill = "*" },
		java		= { head = "/*", tail = "*/", fill = "*" },
		kotlin		= { head = "/*", tail = "*/", fill = "*" },
		scala		= { head = "/*", tail = "*/", fill = "*" },
		go			= { head = "/*", tail = "*/", fill = "*" },
		cs			= { head = "/*", tail = "*/", fill = "*" },
		php			= { head = "/*", tail = "*/", fill = "*" },
		phps		= { head = "/*", tail = "*/", fill = "*" },
		rust		= { head = "/*", tail = "*/", fill = "*" },
		d			= { head = "/*", tail = "*/", fill = "*" },
		css			= { head = "/*", tail = "*/", fill = "*" },
		scss		= { head = "/*", tail = "*/", fill = "*" },
		sass		= { head = "/*", tail = "*/", fill = "*" },
		f90			= { head = "/*", tail = "*/", fill = "*" },
		f95			= { head = "/*", tail = "*/", fill = "*" },
		f03			= { head = "/*", tail = "*/", fill = "*" },

		python		= { head = "#", tail = "#", fill = "#" },
		sh			= { head = "#", tail = "#", fill = "#" },
		r			= { head = "#", tail = "#", fill = "#" },
		nim			= { head = "#", tail = "#", fill = "#" },
		julia		= { head = "#", tail = "#", fill = "#" },
		ruby		= { head = "#", tail = "#", fill = "#" },
		elixir		= { head = "#", tail = "#", fill = "#" },
		cr			= { head = "#", tail = "#", fill = "#" },
		ps1			= { head = "#", tail = "#", fill = "#" },
		psm1		= { head = "#", tail = "#", fill = "#" },
		zig			= { head = "#", tail = "#", fill = "#" },
		yaml		= { head = "#", tail = "#", fill = "#" },
		dosini		= { head = "#", tail = "#", fill = "#" },
		make		= { head = "#", tail = "#", fill = "#" },
		dockerfile	= { head = "#", tail = "#", fill = "#" },

		lua			= { head = "--", tail = "--", fill = "-" },
		haskell		= { head = "--", tail = "--", fill = "-" },

		pug			= { head = "//", tail = "//", fill = "-" },

		vb			= { head = "\'", tail = "\'", fill = "\'" },

		tex			= { head = "%", tail = "%", fill = "-" },

		clojure		= { head = ";;", tail = ";;", fill = ";" },
		lisp		= { head = ";", tail = ";;", fill = ";" },
		asm			= { head = ";", tail = ";;", fill = ";" },

		html		= { head = "<!--", tail = "-->", fill = "-" },
		xml			= { head = "<!--", tail = "-->", fill = "-" },
		vim			= { head = "\"", tail = "\"", fill = "*" },
	}
}
